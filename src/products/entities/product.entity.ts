import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'PD_Name', length: 20, unique: true })
  name: string;

  @Column()
  price: number;
}
