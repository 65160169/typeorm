import { IsNotEmpty, Length } from 'class-validator';

export class CreateProductDto {
  @Length(4, 16)
  @IsNotEmpty()
  name: string;

  @Length(4, 16)
  @IsNotEmpty()
  price: number;
}
